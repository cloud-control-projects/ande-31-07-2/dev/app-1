/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigDecimal: any;
  BigInteger: any;
  Date: any;
  DateTime: any;
  LocalDateTime: any;
  LocalTime: any;
  Long: any;
  Time: any;
  Timestamp: any;
  Url: any;
  Void: any;
};

export type ClientValidationTestEntity = {
  __typename?: "ClientValidationTestEntity";
  businessEmail?: Maybe<Scalars["String"]>;
  email?: Maybe<Scalars["String"]>;
  eulaAccepted: Scalars["Boolean"];
  future?: Maybe<Scalars["Timestamp"]>;
  futureOrPresent?: Maybe<Scalars["Date"]>;
  id?: Maybe<Scalars["ID"]>;
  length?: Maybe<Scalars["String"]>;
  negative: Scalars["Float"];
  negativeOrZero: Scalars["Int"];
  notBlank: Scalars["String"];
  notEmpty: Scalars["String"];
  nullField?: Maybe<Scalars["Int"]>;
  past?: Maybe<Scalars["DateTime"]>;
  pastOrPresent?: Maybe<Scalars["Timestamp"]>;
  pattern?: Maybe<Scalars["String"]>;
  positive?: Maybe<Scalars["Int"]>;
  positiveOrZero?: Maybe<Scalars["Long"]>;
  price?: Maybe<Scalars["BigDecimal"]>;
  quantity?: Maybe<Scalars["Long"]>;
  requiredWithoutDirective: Scalars["String"];
  size?: Maybe<Scalars["String"]>;
  trialExpired?: Maybe<Scalars["Boolean"]>;
  urlString?: Maybe<Scalars["String"]>;
  urlWithoutDirective?: Maybe<Scalars["Url"]>;
};

export type ClientValidationTestEntityInput = {
  businessEmail?: InputMaybe<Scalars["String"]>;
  email?: InputMaybe<Scalars["String"]>;
  eulaAccepted: Scalars["Boolean"];
  future?: InputMaybe<Scalars["Timestamp"]>;
  futureOrPresent?: InputMaybe<Scalars["Date"]>;
  id?: InputMaybe<Scalars["ID"]>;
  length?: InputMaybe<Scalars["String"]>;
  negative: Scalars["Float"];
  negativeOrZero: Scalars["Int"];
  notBlank: Scalars["String"];
  notEmpty: Scalars["String"];
  nullField?: InputMaybe<Scalars["Int"]>;
  past?: InputMaybe<Scalars["DateTime"]>;
  pastOrPresent?: InputMaybe<Scalars["Timestamp"]>;
  pattern?: InputMaybe<Scalars["String"]>;
  positive?: InputMaybe<Scalars["Int"]>;
  positiveOrZero?: InputMaybe<Scalars["Long"]>;
  price?: InputMaybe<Scalars["BigDecimal"]>;
  quantity?: InputMaybe<Scalars["Long"]>;
  requiredWithoutDirective: Scalars["String"];
  size?: InputMaybe<Scalars["String"]>;
  trialExpired?: InputMaybe<Scalars["Boolean"]>;
  urlString?: InputMaybe<Scalars["String"]>;
  urlWithoutDirective?: InputMaybe<Scalars["Url"]>;
};

export enum Direction {
  Asc = "ASC",
  Desc = "DESC",
}

export type Mutation = {
  __typename?: "Mutation";
  deleteClientValidationTestEntity?: Maybe<Scalars["Void"]>;
  deleteManyOwner?: Maybe<Scalars["Void"]>;
  deleteManyPet?: Maybe<Scalars["Void"]>;
  deleteManyPetType?: Maybe<Scalars["Void"]>;
  deleteNotNullScalarsTestEntity?: Maybe<Scalars["Void"]>;
  deleteOrder?: Maybe<Scalars["Void"]>;
  deleteOwner?: Maybe<Scalars["Void"]>;
  deletePet?: Maybe<Scalars["Void"]>;
  deletePetDescription?: Maybe<Scalars["Void"]>;
  deletePetDisease?: Maybe<Scalars["Void"]>;
  deletePetType?: Maybe<Scalars["Void"]>;
  deleteScalarsTestEntity?: Maybe<Scalars["Void"]>;
  deleteTag?: Maybe<Scalars["Void"]>;
  deleteVisit?: Maybe<Scalars["Void"]>;
  postponeVisit?: Maybe<VisitDto>;
  updateClientValidationTestEntity: ClientValidationTestEntity;
  updateManyPet?: Maybe<Array<Maybe<PetDto>>>;
  updateManyPetType?: Maybe<Array<Maybe<PetTypeDto>>>;
  updateNotNullScalarsTestEntity?: Maybe<NotNullScalarsTestEntity>;
  updateOrder: Order;
  updateOwner?: Maybe<OwnerDto>;
  updatePet?: Maybe<PetDto>;
  updatePetDescription: PetDescriptionDto;
  updatePetDisease: PetDiseaseDto;
  updatePetType?: Maybe<PetTypeDto>;
  updateScalarsTestEntity?: Maybe<ScalarsTestEntity>;
  updateTag?: Maybe<TagDto>;
  updateVisit?: Maybe<VisitDto>;
};

export type MutationDeleteClientValidationTestEntityArgs = {
  id: Scalars["ID"];
};

export type MutationDeleteManyOwnerArgs = {
  ids: Array<InputMaybe<Scalars["ID"]>>;
};

export type MutationDeleteManyPetArgs = {
  ids: Array<InputMaybe<Scalars["ID"]>>;
};

export type MutationDeleteManyPetTypeArgs = {
  ids: Array<InputMaybe<Scalars["ID"]>>;
};

export type MutationDeleteNotNullScalarsTestEntityArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type MutationDeleteOrderArgs = {
  id: Scalars["ID"];
};

export type MutationDeleteOwnerArgs = {
  id: Scalars["ID"];
};

export type MutationDeletePetArgs = {
  id: Scalars["ID"];
};

export type MutationDeletePetDescriptionArgs = {
  id: Scalars["ID"];
};

export type MutationDeletePetDiseaseArgs = {
  id: Scalars["ID"];
};

export type MutationDeletePetTypeArgs = {
  id: Scalars["ID"];
};

export type MutationDeleteScalarsTestEntityArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type MutationDeleteTagArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type MutationDeleteVisitArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type MutationPostponeVisitArgs = {
  days: Scalars["Int"];
  visitId: Scalars["ID"];
};

export type MutationUpdateClientValidationTestEntityArgs = {
  input: ClientValidationTestEntityInput;
};

export type MutationUpdateManyPetArgs = {
  inputList: Array<InputMaybe<PetInputDto>>;
};

export type MutationUpdateManyPetTypeArgs = {
  inputList: Array<InputMaybe<PetTypeInputDto>>;
};

export type MutationUpdateNotNullScalarsTestEntityArgs = {
  input?: InputMaybe<NotNullScalarsTestEntityInput>;
};

export type MutationUpdateOrderArgs = {
  input: OrderInput;
};

export type MutationUpdateOwnerArgs = {
  input?: InputMaybe<OwnerInputDto>;
};

export type MutationUpdatePetArgs = {
  input?: InputMaybe<PetInputDto>;
};

export type MutationUpdatePetDescriptionArgs = {
  input: PetDescriptionInputDto;
};

export type MutationUpdatePetDiseaseArgs = {
  input: PetDiseaseInputDto;
};

export type MutationUpdatePetTypeArgs = {
  input?: InputMaybe<PetTypeInputDto>;
};

export type MutationUpdateScalarsTestEntityArgs = {
  input?: InputMaybe<ScalarsTestEntityInput>;
};

export type MutationUpdateTagArgs = {
  input?: InputMaybe<TagInputDto>;
};

export type MutationUpdateVisitArgs = {
  input?: InputMaybe<VisitInputDto>;
};

export type NotNullScalarsTestEntity = {
  __typename?: "NotNullScalarsTestEntity";
  bigDecimalNotNull: Scalars["BigDecimal"];
  bigIntNotNull: Scalars["BigInteger"];
  dateTestNotNull: Scalars["Timestamp"];
  id?: Maybe<Scalars["ID"]>;
  localDateNotNull: Scalars["Date"];
  localDateTimeNotNull: Scalars["LocalDateTime"];
  localTimeNotNull: Scalars["LocalTime"];
  offsetDateTimeNotNull: Scalars["DateTime"];
  offsetTimeNotNull: Scalars["Time"];
  stringNotNull: Scalars["String"];
  urlNotNull: Scalars["Url"];
};

export type NotNullScalarsTestEntityFilterInput = {
  bigDecimalNotNullMin?: InputMaybe<Scalars["BigDecimal"]>;
  bigIntNotNullMin?: InputMaybe<Scalars["BigInteger"]>;
  dateTestNotNullMin?: InputMaybe<Scalars["Timestamp"]>;
  localDateNotNullMax?: InputMaybe<Scalars["Date"]>;
  localDateTimeNotNullMin?: InputMaybe<Scalars["LocalDateTime"]>;
  localTimeNotNull?: InputMaybe<Scalars["LocalTime"]>;
  offsetDateTimeNotNullMax?: InputMaybe<Scalars["DateTime"]>;
  offsetDateTimeNotNullMin?: InputMaybe<Scalars["DateTime"]>;
  offsetTimeNotNullMax?: InputMaybe<Scalars["Time"]>;
  stringNotNull?: InputMaybe<Scalars["String"]>;
  urlNotNull?: InputMaybe<Scalars["Url"]>;
};

export type NotNullScalarsTestEntityInput = {
  bigDecimalNotNull: Scalars["BigDecimal"];
  bigIntNotNull: Scalars["BigInteger"];
  dateTestNotNull: Scalars["Timestamp"];
  id?: InputMaybe<Scalars["ID"]>;
  localDateNotNull: Scalars["Date"];
  localDateTimeNotNull: Scalars["LocalDateTime"];
  localTimeNotNull: Scalars["LocalTime"];
  offsetDateTimeNotNull: Scalars["DateTime"];
  offsetTimeNotNull: Scalars["Time"];
  stringNotNull: Scalars["String"];
  urlNotNull: Scalars["Url"];
};

export type NotNullScalarsTestEntityOrderByInput = {
  direction?: InputMaybe<SortDirection>;
  property?: InputMaybe<NotNullScalarsTestEntityOrderByProperty>;
};

export enum NotNullScalarsTestEntityOrderByProperty {
  BigDecimalNotNull = "BIG_DECIMAL_NOT_NULL",
  BigIntNotNull = "BIG_INT_NOT_NULL",
  DateTestNotNull = "DATE_TEST_NOT_NULL",
  LocalDateNotNull = "LOCAL_DATE_NOT_NULL",
  LocalDateTimeNotNull = "LOCAL_DATE_TIME_NOT_NULL",
  LocalTimeNotNull = "LOCAL_TIME_NOT_NULL",
  OffsetDateTimeNotNull = "OFFSET_DATE_TIME_NOT_NULL",
  OffsetTimeNotNull = "OFFSET_TIME_NOT_NULL",
  StringNotNull = "STRING_NOT_NULL",
  UrlNotNull = "URL_NOT_NULL",
}

export type NotNullScalarsTestEntityResultPage = {
  __typename?: "NotNullScalarsTestEntityResultPage";
  content?: Maybe<Array<Maybe<NotNullScalarsTestEntity>>>;
  totalElements: Scalars["Long"];
};

export type OffsetPageInput = {
  number: Scalars["Int"];
  size: Scalars["Int"];
};

export type Order = {
  __typename?: "Order";
  amount?: Maybe<Scalars["BigDecimal"]>;
  id?: Maybe<Scalars["ID"]>;
  owner?: Maybe<Owner>;
};

export type OrderInput = {
  amount?: InputMaybe<Scalars["BigDecimal"]>;
  id?: InputMaybe<Scalars["ID"]>;
};

export type Owner = {
  __typename?: "Owner";
  address?: Maybe<Scalars["String"]>;
  city?: Maybe<Scalars["String"]>;
  email?: Maybe<Scalars["String"]>;
  firstName: Scalars["String"];
  id?: Maybe<Scalars["ID"]>;
  lastName: Scalars["String"];
  telephone?: Maybe<Scalars["String"]>;
};

export type OwnerDto = {
  __typename?: "OwnerDTO";
  address?: Maybe<Scalars["String"]>;
  city?: Maybe<Scalars["String"]>;
  email?: Maybe<Scalars["String"]>;
  firstName?: Maybe<Scalars["String"]>;
  id?: Maybe<Scalars["ID"]>;
  lastName?: Maybe<Scalars["String"]>;
  telephone?: Maybe<Scalars["String"]>;
};

export type OwnerDtoInput = {
  address?: InputMaybe<Scalars["String"]>;
  city?: InputMaybe<Scalars["String"]>;
  email?: InputMaybe<Scalars["String"]>;
  firstName?: InputMaybe<Scalars["String"]>;
  id?: InputMaybe<Scalars["ID"]>;
  lastName?: InputMaybe<Scalars["String"]>;
  telephone?: InputMaybe<Scalars["String"]>;
};

export type OwnerDtoResultPage = {
  __typename?: "OwnerDTOResultPage";
  content?: Maybe<Array<Maybe<OwnerDto>>>;
  totalElements: Scalars["Long"];
};

export type OwnerFilterInput = {
  firstName?: InputMaybe<Scalars["String"]>;
  lastName?: InputMaybe<Scalars["String"]>;
};

export type OwnerInput = {
  address?: InputMaybe<Scalars["String"]>;
  city?: InputMaybe<Scalars["String"]>;
  email?: InputMaybe<Scalars["String"]>;
  firstName: Scalars["String"];
  id?: InputMaybe<Scalars["ID"]>;
  lastName: Scalars["String"];
  telephone?: InputMaybe<Scalars["String"]>;
};

export type OwnerInputDto = {
  address?: InputMaybe<Scalars["String"]>;
  city?: InputMaybe<Scalars["String"]>;
  email?: InputMaybe<Scalars["String"]>;
  firstName: Scalars["String"];
  id?: InputMaybe<Scalars["ID"]>;
  lastName: Scalars["String"];
  telephone?: InputMaybe<Scalars["String"]>;
};

export type OwnerOrderByInput = {
  direction?: InputMaybe<Direction>;
  property?: InputMaybe<OwnerOrderByProperty>;
};

export enum OwnerOrderByProperty {
  City = "CITY",
  FirstName = "FIRST_NAME",
  LastName = "LAST_NAME",
}

export type OwnerResultPage = {
  __typename?: "OwnerResultPage";
  content?: Maybe<Array<Maybe<Owner>>>;
  totalElements: Scalars["Long"];
};

export type PetDto = {
  __typename?: "PetDTO";
  birthDate?: Maybe<Scalars["Date"]>;
  description?: Maybe<PetDescriptionDto>;
  diseases?: Maybe<Array<Maybe<PetDiseaseDto>>>;
  id?: Maybe<Scalars["ID"]>;
  identificationNumber: Scalars["String"];
  owner?: Maybe<OwnerDto>;
  tags?: Maybe<Array<Maybe<TagDto>>>;
  type?: Maybe<PetTypeDto>;
  weightInGrams?: Maybe<Scalars["Int"]>;
};

export type PetDtoInput = {
  birthDate?: InputMaybe<Scalars["Date"]>;
  description?: InputMaybe<PetDescriptionDtoInput>;
  diseases?: InputMaybe<Array<InputMaybe<PetDiseaseDtoInput>>>;
  id?: InputMaybe<Scalars["ID"]>;
  identificationNumber: Scalars["String"];
  owner?: InputMaybe<OwnerDtoInput>;
  tags?: InputMaybe<Array<InputMaybe<TagDtoInput>>>;
  type?: InputMaybe<PetTypeDtoInput>;
  weightInGrams?: InputMaybe<Scalars["Int"]>;
};

export type PetDescriptionDto = {
  __typename?: "PetDescriptionDTO";
  description?: Maybe<Scalars["String"]>;
  id?: Maybe<Scalars["ID"]>;
};

export type PetDescriptionDtoInput = {
  description?: InputMaybe<Scalars["String"]>;
  id?: InputMaybe<Scalars["ID"]>;
};

export type PetDescriptionInputDto = {
  description?: InputMaybe<Scalars["String"]>;
  id?: InputMaybe<Scalars["ID"]>;
};

export type PetDiseaseDto = {
  __typename?: "PetDiseaseDTO";
  description?: Maybe<Scalars["String"]>;
  id?: Maybe<Scalars["Long"]>;
  name?: Maybe<Scalars["String"]>;
};

export type PetDiseaseDtoInput = {
  description?: InputMaybe<Scalars["String"]>;
  id?: InputMaybe<Scalars["ID"]>;
  name?: InputMaybe<Scalars["String"]>;
};

export type PetDiseaseInputDto = {
  description?: InputMaybe<Scalars["String"]>;
  id?: InputMaybe<Scalars["ID"]>;
  name?: InputMaybe<Scalars["String"]>;
};

export type PetInputDto = {
  birthDate?: InputMaybe<Scalars["Date"]>;
  description?: InputMaybe<PetDescriptionDtoInput>;
  diseases?: InputMaybe<Array<InputMaybe<PetDiseaseDtoInput>>>;
  id?: InputMaybe<Scalars["ID"]>;
  identificationNumber: Scalars["String"];
  owner?: InputMaybe<OwnerDtoInput>;
  tags?: InputMaybe<Array<InputMaybe<TagDtoInput>>>;
  type?: InputMaybe<PetTypeDtoInput>;
  weightInGrams?: InputMaybe<Scalars["Int"]>;
};

export type PetTypeDto = {
  __typename?: "PetTypeDTO";
  defenseStatus?: Maybe<ProtectionStatus>;
  id?: Maybe<Scalars["ID"]>;
  name?: Maybe<Scalars["String"]>;
};

export type PetTypeDtoInput = {
  defenseStatus?: InputMaybe<ProtectionStatus>;
  id?: InputMaybe<Scalars["ID"]>;
  name?: InputMaybe<Scalars["String"]>;
};

export type PetTypeFilterInput = {
  defenseStatus?: InputMaybe<ProtectionStatus>;
  name?: InputMaybe<Scalars["String"]>;
};

export type PetTypeInputDto = {
  defenseStatus?: InputMaybe<ProtectionStatus>;
  id?: InputMaybe<Scalars["ID"]>;
  name?: InputMaybe<Scalars["String"]>;
};

export enum ProtectionStatus {
  NeedsProtection = "NEEDS_PROTECTION",
  NoDanger = "NO_DANGER",
  RedBook = "RED_BOOK",
}

export type Query = {
  __typename?: "Query";
  clientValidationTestEntity: ClientValidationTestEntity;
  clientValidationTestEntityList?: Maybe<
    Array<Maybe<ClientValidationTestEntity>>
  >;
  notNullScalarsTestEntity?: Maybe<NotNullScalarsTestEntity>;
  notNullScalarsTestEntityList: NotNullScalarsTestEntityResultPage;
  notNullScalarsTestEntityListFull?: Maybe<
    Array<Maybe<NotNullScalarsTestEntity>>
  >;
  order: Order;
  orderList?: Maybe<Array<Maybe<Order>>>;
  owner?: Maybe<OwnerDto>;
  ownerByNamesList?: Maybe<Array<Maybe<OwnerDto>>>;
  ownerList?: Maybe<OwnerDtoResultPage>;
  ownerListByIds?: Maybe<Array<Maybe<OwnerDto>>>;
  ownerListByNamesFilter?: Maybe<Array<Maybe<OwnerDto>>>;
  ownerListByNamesFilterOffsetPage?: Maybe<OwnerDtoResultPage>;
  ownerListByNamesFilterSorted?: Maybe<Array<Maybe<OwnerDto>>>;
  ownerListFull?: Maybe<Array<Maybe<OwnerDto>>>;
  ownerListOffsetPage?: Maybe<OwnerDtoResultPage>;
  ownerListOffsetPageSorted?: Maybe<OwnerDtoResultPage>;
  ownerListSorted?: Maybe<Array<Maybe<OwnerDto>>>;
  ownerListWithResultPage: OwnerResultPage;
  pet?: Maybe<PetDto>;
  petByIdentificationNumberList?: Maybe<Array<Maybe<PetDto>>>;
  petDescription: PetDescriptionDto;
  petDescriptionList?: Maybe<Array<Maybe<PetDescriptionDto>>>;
  petDisease: PetDiseaseDto;
  petDiseaseList?: Maybe<Array<Maybe<PetDiseaseDto>>>;
  petList?: Maybe<Array<Maybe<PetDto>>>;
  petListByIds?: Maybe<Array<Maybe<PetDto>>>;
  petListByTypeId?: Maybe<Array<Maybe<PetDto>>>;
  petType?: Maybe<PetTypeDto>;
  petTypeList: Array<Maybe<PetTypeDto>>;
  petTypeListByIds?: Maybe<Array<Maybe<PetTypeDto>>>;
  scalarsTestEntity?: Maybe<ScalarsTestEntity>;
  scalarsTestEntityList: ScalarsTestEntityResultPage;
  scalarsTestEntityListFull?: Maybe<Array<Maybe<ScalarsTestEntity>>>;
  tag?: Maybe<TagDto>;
  tagList?: Maybe<Array<Maybe<TagDto>>>;
  userInfo?: Maybe<UserInfo>;
  userPermissions?: Maybe<Array<Maybe<Scalars["String"]>>>;
  visit?: Maybe<VisitDto>;
  visitList?: Maybe<Array<Maybe<VisitDto>>>;
  visitListFull?: Maybe<Array<Maybe<VisitDto>>>;
};

export type QueryClientValidationTestEntityArgs = {
  id: Scalars["ID"];
};

export type QueryNotNullScalarsTestEntityArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryNotNullScalarsTestEntityListArgs = {
  filter?: InputMaybe<NotNullScalarsTestEntityFilterInput>;
  page?: InputMaybe<OffsetPageInput>;
  sort?: InputMaybe<Array<InputMaybe<NotNullScalarsTestEntityOrderByInput>>>;
};

export type QueryOrderArgs = {
  id: Scalars["ID"];
};

export type QueryOrderListArgs = {
  amount?: InputMaybe<Scalars["BigDecimal"]>;
};

export type QueryOwnerArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryOwnerByNamesListArgs = {
  filter?: InputMaybe<OwnerFilterInput>;
};

export type QueryOwnerListArgs = {
  filter?: InputMaybe<OwnerFilterInput>;
  page?: InputMaybe<OffsetPageInput>;
  sort?: InputMaybe<Array<InputMaybe<OwnerOrderByInput>>>;
};

export type QueryOwnerListByIdsArgs = {
  ids: Array<InputMaybe<Scalars["ID"]>>;
};

export type QueryOwnerListByNamesFilterArgs = {
  filter?: InputMaybe<OwnerFilterInput>;
};

export type QueryOwnerListByNamesFilterOffsetPageArgs = {
  filter?: InputMaybe<OwnerFilterInput>;
  page?: InputMaybe<OffsetPageInput>;
};

export type QueryOwnerListByNamesFilterSortedArgs = {
  filter?: InputMaybe<OwnerFilterInput>;
  sort?: InputMaybe<Array<InputMaybe<OwnerOrderByInput>>>;
};

export type QueryOwnerListOffsetPageArgs = {
  page?: InputMaybe<OffsetPageInput>;
};

export type QueryOwnerListOffsetPageSortedArgs = {
  page?: InputMaybe<OffsetPageInput>;
  sort?: InputMaybe<Array<InputMaybe<OwnerOrderByInput>>>;
};

export type QueryOwnerListSortedArgs = {
  sort?: InputMaybe<Array<InputMaybe<OwnerOrderByInput>>>;
};

export type QueryOwnerListWithResultPageArgs = {
  page?: InputMaybe<OffsetPageInput>;
};

export type QueryPetArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryPetByIdentificationNumberListArgs = {
  identificationNumber?: InputMaybe<Scalars["String"]>;
};

export type QueryPetDescriptionArgs = {
  id: Scalars["ID"];
};

export type QueryPetDiseaseArgs = {
  id: Scalars["ID"];
};

export type QueryPetListByIdsArgs = {
  ids: Array<InputMaybe<Scalars["ID"]>>;
};

export type QueryPetListByTypeIdArgs = {
  typeId: Scalars["ID"];
};

export type QueryPetTypeArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryPetTypeListArgs = {
  filter?: InputMaybe<PetTypeFilterInput>;
};

export type QueryPetTypeListByIdsArgs = {
  ids: Array<InputMaybe<Scalars["ID"]>>;
};

export type QueryScalarsTestEntityArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryScalarsTestEntityListArgs = {
  filter?: InputMaybe<ScalarsTestEntityFilterInput>;
  page?: InputMaybe<OffsetPageInput>;
  sort?: InputMaybe<Array<InputMaybe<ScalarsTestEntityOrderByInput>>>;
};

export type QueryTagArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryVisitArgs = {
  id?: InputMaybe<Scalars["ID"]>;
};

export type QueryVisitListArgs = {
  filter?: InputMaybe<VisitFilterInput>;
};

export type ScalarsTestEntity = {
  __typename?: "ScalarsTestEntity";
  bigDecimal?: Maybe<Scalars["BigDecimal"]>;
  bigInt?: Maybe<Scalars["BigInteger"]>;
  bool?: Maybe<Scalars["Boolean"]>;
  boolPrimitive: Scalars["Boolean"];
  bytePrimitive: Scalars["Int"];
  byteTest?: Maybe<Scalars["Int"]>;
  dateTest?: Maybe<Scalars["Timestamp"]>;
  doublePrimitive: Scalars["Float"];
  doubleTest?: Maybe<Scalars["Float"]>;
  floatPrimitive: Scalars["Float"];
  floatTest?: Maybe<Scalars["Float"]>;
  id?: Maybe<Scalars["ID"]>;
  intPrimitive: Scalars["Int"];
  intTest?: Maybe<Scalars["Int"]>;
  localDate?: Maybe<Scalars["Date"]>;
  localDateTime?: Maybe<Scalars["LocalDateTime"]>;
  localTime?: Maybe<Scalars["LocalTime"]>;
  longPrimitive: Scalars["Long"];
  longTest?: Maybe<Scalars["Long"]>;
  offsetDateTime?: Maybe<Scalars["DateTime"]>;
  offsetTime?: Maybe<Scalars["Time"]>;
  shortPrimitive: Scalars["Int"];
  shortTest?: Maybe<Scalars["Int"]>;
  string?: Maybe<Scalars["String"]>;
  url?: Maybe<Scalars["Url"]>;
};

export type ScalarsTestEntityFilterInput = {
  bigDecimal?: InputMaybe<Scalars["BigDecimal"]>;
  bigInt?: InputMaybe<Scalars["BigInteger"]>;
  bool?: InputMaybe<Scalars["Boolean"]>;
  byteTest?: InputMaybe<Scalars["Int"]>;
  dateTest?: InputMaybe<Scalars["Timestamp"]>;
  doubleTest?: InputMaybe<Scalars["Float"]>;
  floatTest?: InputMaybe<Scalars["Float"]>;
  intTest?: InputMaybe<Scalars["Int"]>;
  localDate?: InputMaybe<Scalars["Date"]>;
  localDateTime?: InputMaybe<Scalars["LocalDateTime"]>;
  localTime?: InputMaybe<Scalars["LocalTime"]>;
  longTest?: InputMaybe<Scalars["Long"]>;
  offsetDateTime?: InputMaybe<Scalars["DateTime"]>;
  offsetTime?: InputMaybe<Scalars["Time"]>;
  shortTest?: InputMaybe<Scalars["Int"]>;
  string?: InputMaybe<Scalars["String"]>;
  url?: InputMaybe<Scalars["Url"]>;
};

export type ScalarsTestEntityInput = {
  bigDecimal?: InputMaybe<Scalars["BigDecimal"]>;
  bigInt?: InputMaybe<Scalars["BigInteger"]>;
  bool?: InputMaybe<Scalars["Boolean"]>;
  boolPrimitive: Scalars["Boolean"];
  bytePrimitive: Scalars["Int"];
  byteTest?: InputMaybe<Scalars["Int"]>;
  dateTest?: InputMaybe<Scalars["Timestamp"]>;
  doublePrimitive: Scalars["Float"];
  doubleTest?: InputMaybe<Scalars["Float"]>;
  floatPrimitive: Scalars["Float"];
  floatTest?: InputMaybe<Scalars["Float"]>;
  id?: InputMaybe<Scalars["ID"]>;
  intPrimitive: Scalars["Int"];
  intTest?: InputMaybe<Scalars["Int"]>;
  localDate?: InputMaybe<Scalars["Date"]>;
  localDateTime?: InputMaybe<Scalars["LocalDateTime"]>;
  localTime?: InputMaybe<Scalars["LocalTime"]>;
  longPrimitive: Scalars["Long"];
  longTest?: InputMaybe<Scalars["Long"]>;
  offsetDateTime?: InputMaybe<Scalars["DateTime"]>;
  offsetTime?: InputMaybe<Scalars["Time"]>;
  shortPrimitive: Scalars["Int"];
  shortTest?: InputMaybe<Scalars["Int"]>;
  string?: InputMaybe<Scalars["String"]>;
  url?: InputMaybe<Scalars["Url"]>;
};

export type ScalarsTestEntityOrderByInput = {
  direction?: InputMaybe<SortDirection>;
  property?: InputMaybe<ScalarsTestEntityOrderByProperty>;
};

export enum ScalarsTestEntityOrderByProperty {
  BigDecimal = "BIG_DECIMAL",
  BigInt = "BIG_INT",
  Bool = "BOOL",
  BoolPrimitive = "BOOL_PRIMITIVE",
  BytePrimitive = "BYTE_PRIMITIVE",
  ByteTest = "BYTE_TEST",
  DateTest = "DATE_TEST",
  DoublePrimitive = "DOUBLE_PRIMITIVE",
  DoubleTest = "DOUBLE_TEST",
  FloatPrimitive = "FLOAT_PRIMITIVE",
  FloatTest = "FLOAT_TEST",
  IntPrimitive = "INT_PRIMITIVE",
  IntTest = "INT_TEST",
  LocalDate = "LOCAL_DATE",
  LocalDateTime = "LOCAL_DATE_TIME",
  LocalTime = "LOCAL_TIME",
  LongPrimitive = "LONG_PRIMITIVE",
  LongTest = "LONG_TEST",
  OffsetDateTime = "OFFSET_DATE_TIME",
  OffsetTime = "OFFSET_TIME",
  ShortPrimitive = "SHORT_PRIMITIVE",
  ShortTest = "SHORT_TEST",
  String = "STRING",
  Url = "URL",
}

export type ScalarsTestEntityResultPage = {
  __typename?: "ScalarsTestEntityResultPage";
  content?: Maybe<Array<Maybe<ScalarsTestEntity>>>;
  totalElements: Scalars["Long"];
};

export enum SortDirection {
  Asc = "ASC",
  Desc = "DESC",
}

export type TagDto = {
  __typename?: "TagDTO";
  id?: Maybe<Scalars["ID"]>;
  name?: Maybe<Scalars["String"]>;
};

export type TagDtoInput = {
  id?: InputMaybe<Scalars["ID"]>;
  name?: InputMaybe<Scalars["String"]>;
};

export type TagInputDto = {
  id?: InputMaybe<Scalars["ID"]>;
  name?: InputMaybe<Scalars["String"]>;
};

export type TransientPetDtoInput = {
  birthDate?: InputMaybe<Scalars["Date"]>;
  description?: InputMaybe<PetDescriptionDtoInput>;
  diseases?: InputMaybe<Array<InputMaybe<PetDiseaseDtoInput>>>;
  id?: InputMaybe<Scalars["ID"]>;
  identificationNumber?: InputMaybe<Scalars["String"]>;
  owner?: InputMaybe<OwnerDtoInput>;
  tags?: InputMaybe<Array<InputMaybe<TagDtoInput>>>;
  type?: InputMaybe<PetTypeDtoInput>;
  weightInGrams?: InputMaybe<Scalars["Int"]>;
};

export type UserInfo = {
  __typename?: "UserInfo";
  username?: Maybe<Scalars["String"]>;
};

export type VisitDto = {
  __typename?: "VisitDTO";
  description?: Maybe<Scalars["String"]>;
  id?: Maybe<Scalars["ID"]>;
  pet: PetDto;
  visitEnd?: Maybe<Scalars["LocalDateTime"]>;
  visitStart?: Maybe<Scalars["LocalDateTime"]>;
};

export type VisitFilterInput = {
  ownerFirstName?: InputMaybe<Scalars["String"]>;
  ownerLastName?: InputMaybe<Scalars["String"]>;
  petIdentificationNumber?: InputMaybe<Scalars["String"]>;
  visitStartAfter?: InputMaybe<Scalars["LocalDateTime"]>;
  visitStartBefore?: InputMaybe<Scalars["LocalDateTime"]>;
};

export type VisitInputDto = {
  description?: InputMaybe<Scalars["String"]>;
  id?: InputMaybe<Scalars["ID"]>;
  pet?: InputMaybe<TransientPetDtoInput>;
  visitEnd?: InputMaybe<Scalars["LocalDateTime"]>;
  visitStart?: InputMaybe<Scalars["LocalDateTime"]>;
};

export type Get_Owner_ListQueryVariables = Exact<{ [key: string]: never }>;

export type Get_Owner_ListQuery = {
  __typename?: "Query";
  ownerListFull?: Array<{
    __typename?: "OwnerDTO";
    id?: string | null;
    firstName?: string | null;
    lastName?: string | null;
    city?: string | null;
    address?: string | null;
    telephone?: string | null;
    email?: string | null;
  } | null> | null;
};

export type Get_Pet_Description_ListQueryVariables = Exact<{
  [key: string]: never;
}>;

export type Get_Pet_Description_ListQuery = {
  __typename?: "Query";
  petDescriptionList?: Array<{
    __typename?: "PetDescriptionDTO";
    id?: string | null;
    description?: string | null;
  } | null> | null;
};

export type Get_Pet_Disease_ListQueryVariables = Exact<{
  [key: string]: never;
}>;

export type Get_Pet_Disease_ListQuery = {
  __typename?: "Query";
  petDiseaseList?: Array<{
    __typename?: "PetDiseaseDTO";
    description?: string | null;
    name?: string | null;
    id?: any | null;
  } | null> | null;
};

export type Get_Pet_ListQueryVariables = Exact<{ [key: string]: never }>;

export type Get_Pet_ListQuery = {
  __typename?: "Query";
  petList?: Array<{
    __typename?: "PetDTO";
    id?: string | null;
    identificationNumber: string;
    birthDate?: any | null;
    type?: {
      __typename?: "PetTypeDTO";
      id?: string | null;
      name?: string | null;
    } | null;
    owner?: {
      __typename?: "OwnerDTO";
      id?: string | null;
      firstName?: string | null;
      lastName?: string | null;
    } | null;
    description?: {
      __typename?: "PetDescriptionDTO";
      id?: string | null;
      description?: string | null;
    } | null;
    tags?: Array<{
      __typename?: "TagDTO";
      id?: string | null;
      name?: string | null;
    } | null> | null;
    diseases?: Array<{
      __typename?: "PetDiseaseDTO";
      id?: any | null;
      name?: string | null;
      description?: string | null;
    } | null> | null;
  } | null> | null;
};

export type Get_Pet_Type_ListQueryVariables = Exact<{ [key: string]: never }>;

export type Get_Pet_Type_ListQuery = {
  __typename?: "Query";
  petTypeList: Array<{
    __typename?: "PetTypeDTO";
    id?: string | null;
    name?: string | null;
    defenseStatus?: ProtectionStatus | null;
  } | null>;
};

export type Get_Scalars_ListQueryVariables = Exact<{ [key: string]: never }>;

export type Get_Scalars_ListQuery = {
  __typename?: "Query";
  scalarsTestEntityListFull?: Array<{
    __typename?: "ScalarsTestEntity";
    id?: string | null;
    intTest?: number | null;
    intPrimitive: number;
    byteTest?: number | null;
    bytePrimitive: number;
    shortTest?: number | null;
    shortPrimitive: number;
    doubleTest?: number | null;
    doublePrimitive: number;
    floatTest?: number | null;
    floatPrimitive: number;
    string?: string | null;
    bool?: boolean | null;
    boolPrimitive: boolean;
    bigInt?: any | null;
    longTest?: any | null;
    longPrimitive: any;
    bigDecimal?: any | null;
    localDate?: any | null;
    localDateTime?: any | null;
    localTime?: any | null;
    offsetDateTime?: any | null;
    offsetTime?: any | null;
    dateTest?: any | null;
    url?: any | null;
  } | null> | null;
};

export type Get_Nn_Scalars_ListQueryVariables = Exact<{ [key: string]: never }>;

export type Get_Nn_Scalars_ListQuery = {
  __typename?: "Query";
  notNullScalarsTestEntityListFull?: Array<{
    __typename?: "NotNullScalarsTestEntity";
    id?: string | null;
    bigDecimalNotNull: any;
    bigIntNotNull: any;
    dateTestNotNull: any;
    localDateNotNull: any;
    localDateTimeNotNull: any;
    localTimeNotNull: any;
    offsetDateTimeNotNull: any;
    offsetTimeNotNull: any;
    stringNotNull: string;
    urlNotNull: any;
  } | null> | null;
};

export type Get_Client_Validation_Test_Entity_ListQueryVariables = Exact<{
  [key: string]: never;
}>;

export type Get_Client_Validation_Test_Entity_ListQuery = {
  __typename?: "Query";
  clientValidationTestEntityList?: Array<{
    __typename?: "ClientValidationTestEntity";
    id?: string | null;
    businessEmail?: string | null;
    email?: string | null;
    eulaAccepted: boolean;
    future?: any | null;
    futureOrPresent?: any | null;
    length?: string | null;
    negative: number;
    negativeOrZero: number;
    notBlank: string;
    notEmpty: string;
    nullField?: number | null;
    past?: any | null;
    pastOrPresent?: any | null;
    pattern?: string | null;
    positive?: number | null;
    positiveOrZero?: any | null;
    price?: any | null;
    quantity?: any | null;
    requiredWithoutDirective: string;
    size?: string | null;
    trialExpired?: boolean | null;
    urlString?: string | null;
    urlWithoutDirective?: any | null;
  } | null> | null;
};

export type Delete_Client_Validation_Test_EntityMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_Client_Validation_Test_EntityMutation = {
  __typename?: "Mutation";
  deleteClientValidationTestEntity?: any | null;
};

export type Get_Client_Validation_Test_EntityQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_Client_Validation_Test_EntityQuery = {
  __typename?: "Query";
  clientValidationTestEntity: {
    __typename?: "ClientValidationTestEntity";
    id?: string | null;
    businessEmail?: string | null;
    email?: string | null;
    eulaAccepted: boolean;
    future?: any | null;
    futureOrPresent?: any | null;
    length?: string | null;
    negative: number;
    negativeOrZero: number;
    notBlank: string;
    notEmpty: string;
    nullField?: number | null;
    past?: any | null;
    pastOrPresent?: any | null;
    pattern?: string | null;
    positive?: number | null;
    positiveOrZero?: any | null;
    price?: any | null;
    quantity?: any | null;
    requiredWithoutDirective: string;
    size?: string | null;
    trialExpired?: boolean | null;
    urlString?: string | null;
    urlWithoutDirective?: any | null;
  };
};

export type Update_Client_Validation_Test_EntityMutationVariables = Exact<{
  input: ClientValidationTestEntityInput;
}>;

export type Update_Client_Validation_Test_EntityMutation = {
  __typename?: "Mutation";
  updateClientValidationTestEntity: {
    __typename?: "ClientValidationTestEntity";
    id?: string | null;
  };
};

export type Delete_OwnerMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_OwnerMutation = {
  __typename?: "Mutation";
  deleteOwner?: any | null;
};

export type Get_OwnerQueryVariables = Exact<{
  id?: InputMaybe<Scalars["ID"]>;
}>;

export type Get_OwnerQuery = {
  __typename?: "Query";
  owner?: {
    __typename?: "OwnerDTO";
    id?: string | null;
    firstName?: string | null;
    lastName?: string | null;
    city?: string | null;
    address?: string | null;
    telephone?: string | null;
    email?: string | null;
  } | null;
};

export type Update_OwnerMutationVariables = Exact<{
  input?: InputMaybe<OwnerInputDto>;
}>;

export type Update_OwnerMutation = {
  __typename?: "Mutation";
  updateOwner?: { __typename?: "OwnerDTO"; id?: string | null } | null;
};

export type Get_Owner_List_With_FilterQueryVariables = Exact<{
  filter?: InputMaybe<OwnerFilterInput>;
}>;

export type Get_Owner_List_With_FilterQuery = {
  __typename?: "Query";
  ownerByNamesList?: Array<{
    __typename?: "OwnerDTO";
    id?: string | null;
    firstName?: string | null;
    lastName?: string | null;
    city?: string | null;
    address?: string | null;
    telephone?: string | null;
    email?: string | null;
  } | null> | null;
};

export type Get_Owner_List_With_Filter_Page_SortQueryVariables = Exact<{
  filter?: InputMaybe<OwnerFilterInput>;
  page?: InputMaybe<OffsetPageInput>;
  sort?: InputMaybe<
    Array<InputMaybe<OwnerOrderByInput>> | InputMaybe<OwnerOrderByInput>
  >;
}>;

export type Get_Owner_List_With_Filter_Page_SortQuery = {
  __typename?: "Query";
  ownerList?: {
    __typename?: "OwnerDTOResultPage";
    totalElements: any;
    content?: Array<{
      __typename?: "OwnerDTO";
      id?: string | null;
      firstName?: string | null;
      lastName?: string | null;
      city?: string | null;
      address?: string | null;
      telephone?: string | null;
      email?: string | null;
    } | null> | null;
  } | null;
};

export type Get_Owner_List_With_PaginationQueryVariables = Exact<{
  page?: InputMaybe<OffsetPageInput>;
}>;

export type Get_Owner_List_With_PaginationQuery = {
  __typename?: "Query";
  ownerListOffsetPage?: {
    __typename?: "OwnerDTOResultPage";
    totalElements: any;
    content?: Array<{
      __typename?: "OwnerDTO";
      id?: string | null;
      firstName?: string | null;
      lastName?: string | null;
      city?: string | null;
      address?: string | null;
      telephone?: string | null;
      email?: string | null;
    } | null> | null;
  } | null;
};

export type Get_Owner_List_With_SortQueryVariables = Exact<{
  sort?: InputMaybe<
    Array<InputMaybe<OwnerOrderByInput>> | InputMaybe<OwnerOrderByInput>
  >;
}>;

export type Get_Owner_List_With_SortQuery = {
  __typename?: "Query";
  ownerListSorted?: Array<{
    __typename?: "OwnerDTO";
    id?: string | null;
    firstName?: string | null;
    lastName?: string | null;
    city?: string | null;
    address?: string | null;
    telephone?: string | null;
    email?: string | null;
  } | null> | null;
};

export type Get_Pet_List_With_FilterQueryVariables = Exact<{
  identificationNumber?: InputMaybe<Scalars["String"]>;
}>;

export type Get_Pet_List_With_FilterQuery = {
  __typename?: "Query";
  petByIdentificationNumberList?: Array<{
    __typename?: "PetDTO";
    id?: string | null;
    identificationNumber: string;
    birthDate?: any | null;
    type?: {
      __typename?: "PetTypeDTO";
      id?: string | null;
      name?: string | null;
    } | null;
    owner?: {
      __typename?: "OwnerDTO";
      id?: string | null;
      firstName?: string | null;
      lastName?: string | null;
    } | null;
    description?: {
      __typename?: "PetDescriptionDTO";
      id?: string | null;
      description?: string | null;
    } | null;
    tags?: Array<{
      __typename?: "TagDTO";
      id?: string | null;
      name?: string | null;
    } | null> | null;
    diseases?: Array<{
      __typename?: "PetDiseaseDTO";
      id?: any | null;
      name?: string | null;
      description?: string | null;
    } | null> | null;
  } | null> | null;
};

export type Delete_PetMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_PetMutation = {
  __typename?: "Mutation";
  deletePet?: any | null;
};

export type Get_PetQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_PetQuery = {
  __typename?: "Query";
  pet?: {
    __typename?: "PetDTO";
    id?: string | null;
    identificationNumber: string;
    birthDate?: any | null;
    type?: {
      __typename?: "PetTypeDTO";
      id?: string | null;
      name?: string | null;
    } | null;
    owner?: {
      __typename?: "OwnerDTO";
      id?: string | null;
      firstName?: string | null;
      lastName?: string | null;
    } | null;
    description?: {
      __typename?: "PetDescriptionDTO";
      id?: string | null;
      description?: string | null;
    } | null;
    tags?: Array<{
      __typename?: "TagDTO";
      id?: string | null;
      name?: string | null;
    } | null> | null;
    diseases?: Array<{
      __typename?: "PetDiseaseDTO";
      id?: any | null;
      name?: string | null;
      description?: string | null;
    } | null> | null;
  } | null;
};

export type Update_PetMutationVariables = Exact<{
  input?: InputMaybe<PetInputDto>;
}>;

export type Update_PetMutation = {
  __typename?: "Mutation";
  updatePet?: { __typename?: "PetDTO"; id?: string | null } | null;
};

export type Delete_Pet_DiseaseMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_Pet_DiseaseMutation = {
  __typename?: "Mutation";
  deletePetDisease?: any | null;
};

export type Get_Pet_Disease_CardsEditorQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_Pet_Disease_CardsEditorQuery = {
  __typename?: "Query";
  petDisease: {
    __typename?: "PetDiseaseDTO";
    description?: string | null;
    name?: string | null;
    id?: any | null;
  };
};

export type Update_Pet_Disease_CardsEditorMutationVariables = Exact<{
  input: PetDiseaseInputDto;
}>;

export type Update_Pet_Disease_CardsEditorMutation = {
  __typename?: "Mutation";
  updatePetDisease: { __typename?: "PetDiseaseDTO"; id?: any | null };
};

export type Get_Pet_Disease_ListEditorQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_Pet_Disease_ListEditorQuery = {
  __typename?: "Query";
  petDisease: {
    __typename?: "PetDiseaseDTO";
    description?: string | null;
    name?: string | null;
    id?: any | null;
  };
};

export type Update_Pet_Disease_ListEditorMutationVariables = Exact<{
  input: PetDiseaseInputDto;
}>;

export type Update_Pet_Disease_ListEditorMutation = {
  __typename?: "Mutation";
  updatePetDisease: { __typename?: "PetDiseaseDTO"; id?: any | null };
};

export type Get_Pet_Disease_TableEditorQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_Pet_Disease_TableEditorQuery = {
  __typename?: "Query";
  petDisease: {
    __typename?: "PetDiseaseDTO";
    description?: string | null;
    name?: string | null;
    id?: any | null;
  };
};

export type Update_Pet_Disease_TableEditorMutationVariables = Exact<{
  input: PetDiseaseInputDto;
}>;

export type Update_Pet_Disease_TableEditorMutation = {
  __typename?: "Mutation";
  updatePetDisease: { __typename?: "PetDiseaseDTO"; id?: any | null };
};

export type Delete_Pet_TypeMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_Pet_TypeMutation = {
  __typename?: "Mutation";
  deletePetType?: any | null;
};

export type Get_Pet_TypeQueryVariables = Exact<{
  id?: InputMaybe<Scalars["ID"]>;
}>;

export type Get_Pet_TypeQuery = {
  __typename?: "Query";
  petType?: {
    __typename?: "PetTypeDTO";
    id?: string | null;
    name?: string | null;
    defenseStatus?: ProtectionStatus | null;
  } | null;
};

export type Update_Pet_TypeMutationVariables = Exact<{
  input?: InputMaybe<PetTypeInputDto>;
}>;

export type Update_Pet_TypeMutation = {
  __typename?: "Mutation";
  updatePetType?: { __typename?: "PetTypeDTO"; id?: string | null } | null;
};

export type Get_Scalars_Test_Entity_List_With_Filter_Page_SortQueryVariables =
  Exact<{
    filter?: InputMaybe<ScalarsTestEntityFilterInput>;
    page?: InputMaybe<OffsetPageInput>;
    sort?: InputMaybe<
      | Array<InputMaybe<ScalarsTestEntityOrderByInput>>
      | InputMaybe<ScalarsTestEntityOrderByInput>
    >;
  }>;

export type Get_Scalars_Test_Entity_List_With_Filter_Page_SortQuery = {
  __typename?: "Query";
  scalarsTestEntityList: {
    __typename?: "ScalarsTestEntityResultPage";
    totalElements: any;
    content?: Array<{
      __typename?: "ScalarsTestEntity";
      id?: string | null;
      intTest?: number | null;
      intPrimitive: number;
      byteTest?: number | null;
      bytePrimitive: number;
      shortTest?: number | null;
      shortPrimitive: number;
      doubleTest?: number | null;
      doublePrimitive: number;
      floatTest?: number | null;
      floatPrimitive: number;
      string?: string | null;
      bool?: boolean | null;
      boolPrimitive: boolean;
      bigInt?: any | null;
      longTest?: any | null;
      longPrimitive: any;
      bigDecimal?: any | null;
      localDate?: any | null;
      localDateTime?: any | null;
      localTime?: any | null;
      offsetDateTime?: any | null;
      offsetTime?: any | null;
      dateTest?: any | null;
      url?: any | null;
    } | null> | null;
  };
};

export type Delete_ScalarsMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_ScalarsMutation = {
  __typename?: "Mutation";
  deleteScalarsTestEntity?: any | null;
};

export type Get_ScalarsQueryVariables = Exact<{
  id?: InputMaybe<Scalars["ID"]>;
}>;

export type Get_ScalarsQuery = {
  __typename?: "Query";
  scalarsTestEntity?: {
    __typename?: "ScalarsTestEntity";
    id?: string | null;
    intTest?: number | null;
    intPrimitive: number;
    byteTest?: number | null;
    bytePrimitive: number;
    shortTest?: number | null;
    shortPrimitive: number;
    doubleTest?: number | null;
    doublePrimitive: number;
    floatTest?: number | null;
    floatPrimitive: number;
    string?: string | null;
    bool?: boolean | null;
    boolPrimitive: boolean;
    bigInt?: any | null;
    longTest?: any | null;
    longPrimitive: any;
    bigDecimal?: any | null;
    localDate?: any | null;
    localDateTime?: any | null;
    localTime?: any | null;
    offsetDateTime?: any | null;
    offsetTime?: any | null;
    dateTest?: any | null;
    url?: any | null;
  } | null;
};

export type Update_ScalarsMutationVariables = Exact<{
  input?: InputMaybe<ScalarsTestEntityInput>;
}>;

export type Update_ScalarsMutation = {
  __typename?: "Mutation";
  updateScalarsTestEntity?: {
    __typename?: "ScalarsTestEntity";
    id?: string | null;
  } | null;
};

export type Delete_Nn_ScalarsMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_Nn_ScalarsMutation = {
  __typename?: "Mutation";
  deleteNotNullScalarsTestEntity?: any | null;
};

export type Get_Nn_ScalarsQueryVariables = Exact<{
  id?: InputMaybe<Scalars["ID"]>;
}>;

export type Get_Nn_ScalarsQuery = {
  __typename?: "Query";
  notNullScalarsTestEntity?: {
    __typename?: "NotNullScalarsTestEntity";
    id?: string | null;
    bigDecimalNotNull: any;
    bigIntNotNull: any;
    dateTestNotNull: any;
    localDateNotNull: any;
    localDateTimeNotNull: any;
    localTimeNotNull: any;
    offsetDateTimeNotNull: any;
    offsetTimeNotNull: any;
    stringNotNull: string;
    urlNotNull: any;
  } | null;
};

export type Update_Nn_ScalarsMutationVariables = Exact<{
  input?: InputMaybe<NotNullScalarsTestEntityInput>;
}>;

export type Update_Nn_ScalarsMutation = {
  __typename?: "Mutation";
  updateNotNullScalarsTestEntity?: {
    __typename?: "NotNullScalarsTestEntity";
    id?: string | null;
  } | null;
};

export type Get_Nn_Scalars_Test_Entity_List_With_Filter_Page_SortQueryVariables =
  Exact<{
    filter?: InputMaybe<NotNullScalarsTestEntityFilterInput>;
    page?: InputMaybe<OffsetPageInput>;
    sort?: InputMaybe<
      | Array<InputMaybe<NotNullScalarsTestEntityOrderByInput>>
      | InputMaybe<NotNullScalarsTestEntityOrderByInput>
    >;
  }>;

export type Get_Nn_Scalars_Test_Entity_List_With_Filter_Page_SortQuery = {
  __typename?: "Query";
  notNullScalarsTestEntityList: {
    __typename?: "NotNullScalarsTestEntityResultPage";
    totalElements: any;
    content?: Array<{
      __typename?: "NotNullScalarsTestEntity";
      id?: string | null;
      bigDecimalNotNull: any;
      bigIntNotNull: any;
      dateTestNotNull: any;
      localDateNotNull: any;
      localDateTimeNotNull: any;
      localTimeNotNull: any;
      offsetDateTimeNotNull: any;
      offsetTimeNotNull: any;
      stringNotNull: string;
      urlNotNull: any;
    } | null> | null;
  };
};

export type Get_Visit_List_With_FilterQueryVariables = Exact<{
  filter?: InputMaybe<VisitFilterInput>;
}>;

export type Get_Visit_List_With_FilterQuery = {
  __typename?: "Query";
  visitList?: Array<{
    __typename?: "VisitDTO";
    description?: string | null;
    id?: string | null;
    visitEnd?: any | null;
    visitStart?: any | null;
    pet: {
      __typename?: "PetDTO";
      id?: string | null;
      identificationNumber: string;
      birthDate?: any | null;
      type?: {
        __typename?: "PetTypeDTO";
        id?: string | null;
        name?: string | null;
      } | null;
      owner?: {
        __typename?: "OwnerDTO";
        id?: string | null;
        firstName?: string | null;
        lastName?: string | null;
      } | null;
    };
  } | null> | null;
};

export type Delete_VisitMutationVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Delete_VisitMutation = {
  __typename?: "Mutation";
  deleteVisit?: any | null;
};

export type Get_Pet_Disease_ListDetailsQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_Pet_Disease_ListDetailsQuery = {
  __typename?: "Query";
  petDisease: {
    __typename?: "PetDiseaseDTO";
    description?: string | null;
    name?: string | null;
    id?: any | null;
  };
};

export type Get_Owner_List_With_Result_PageQueryVariables = Exact<{
  page?: InputMaybe<OffsetPageInput>;
}>;

export type Get_Owner_List_With_Result_PageQuery = {
  __typename?: "Query";
  ownerListWithResultPage: {
    __typename?: "OwnerResultPage";
    totalElements: any;
    content?: Array<{
      __typename?: "Owner";
      id?: string | null;
      firstName: string;
      lastName: string;
      city?: string | null;
      address?: string | null;
      telephone?: string | null;
      email?: string | null;
    } | null> | null;
  };
};

export type Get_Pet_Disease_StandaloneEditorQueryVariables = Exact<{
  id: Scalars["ID"];
}>;

export type Get_Pet_Disease_StandaloneEditorQuery = {
  __typename?: "Query";
  petDisease: {
    __typename?: "PetDiseaseDTO";
    description?: string | null;
    name?: string | null;
    id?: any | null;
  };
};

export type Update_Pet_Disease_StandaloneEditorMutationVariables = Exact<{
  input: PetDiseaseInputDto;
}>;

export type Update_Pet_Disease_StandaloneEditorMutation = {
  __typename?: "Mutation";
  updatePetDisease: { __typename?: "PetDiseaseDTO"; id?: any | null };
};

export const Get_Owner_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "ownerListFull" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "firstName" } },
                { kind: "Field", name: { kind: "Name", value: "lastName" } },
                { kind: "Field", name: { kind: "Name", value: "city" } },
                { kind: "Field", name: { kind: "Name", value: "address" } },
                { kind: "Field", name: { kind: "Name", value: "telephone" } },
                { kind: "Field", name: { kind: "Name", value: "email" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_Owner_ListQuery, Get_Owner_ListQueryVariables>;
export const Get_Pet_Description_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Description_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDescriptionList" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "description" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Description_ListQuery,
  Get_Pet_Description_ListQueryVariables
>;
export const Get_Pet_Disease_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Disease_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDiseaseList" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Disease_ListQuery,
  Get_Pet_Disease_ListQueryVariables
>;
export const Get_Pet_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petList" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "identificationNumber" },
                },
                { kind: "Field", name: { kind: "Name", value: "birthDate" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "type" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "owner" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "firstName" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "lastName" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "description" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "description" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tags" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "diseases" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "description" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_Pet_ListQuery, Get_Pet_ListQueryVariables>;
export const Get_Pet_Type_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Type_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petTypeList" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "defenseStatus" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Type_ListQuery,
  Get_Pet_Type_ListQueryVariables
>;
export const Get_Scalars_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Scalars_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "scalarsTestEntityListFull" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "intTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "intPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "byteTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "bytePrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "shortTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "shortPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "doubleTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "doublePrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "floatTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "floatPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "string" } },
                { kind: "Field", name: { kind: "Name", value: "bool" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "boolPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "bigInt" } },
                { kind: "Field", name: { kind: "Name", value: "longTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "longPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "bigDecimal" } },
                { kind: "Field", name: { kind: "Name", value: "localDate" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localDateTime" },
                },
                { kind: "Field", name: { kind: "Name", value: "localTime" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "offsetDateTime" },
                },
                { kind: "Field", name: { kind: "Name", value: "offsetTime" } },
                { kind: "Field", name: { kind: "Name", value: "dateTest" } },
                { kind: "Field", name: { kind: "Name", value: "url" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Scalars_ListQuery,
  Get_Scalars_ListQueryVariables
>;
export const Get_Nn_Scalars_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_NN_Scalars_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "notNullScalarsTestEntityListFull" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "bigDecimalNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "bigIntNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "dateTestNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localDateNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localDateTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "offsetDateTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "offsetTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "stringNotNull" },
                },
                { kind: "Field", name: { kind: "Name", value: "urlNotNull" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Nn_Scalars_ListQuery,
  Get_Nn_Scalars_ListQueryVariables
>;
export const Get_Client_Validation_Test_Entity_ListDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Client_Validation_Test_Entity_List" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "clientValidationTestEntityList" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "businessEmail" },
                },
                { kind: "Field", name: { kind: "Name", value: "email" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "eulaAccepted" },
                },
                { kind: "Field", name: { kind: "Name", value: "future" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "futureOrPresent" },
                },
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "length" } },
                { kind: "Field", name: { kind: "Name", value: "negative" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "negativeOrZero" },
                },
                { kind: "Field", name: { kind: "Name", value: "notBlank" } },
                { kind: "Field", name: { kind: "Name", value: "notEmpty" } },
                { kind: "Field", name: { kind: "Name", value: "nullField" } },
                { kind: "Field", name: { kind: "Name", value: "past" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "pastOrPresent" },
                },
                { kind: "Field", name: { kind: "Name", value: "pattern" } },
                { kind: "Field", name: { kind: "Name", value: "positive" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "positiveOrZero" },
                },
                { kind: "Field", name: { kind: "Name", value: "price" } },
                { kind: "Field", name: { kind: "Name", value: "quantity" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "requiredWithoutDirective" },
                },
                { kind: "Field", name: { kind: "Name", value: "size" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "trialExpired" },
                },
                { kind: "Field", name: { kind: "Name", value: "urlString" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "urlWithoutDirective" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Client_Validation_Test_Entity_ListQuery,
  Get_Client_Validation_Test_Entity_ListQueryVariables
>;
export const Delete_Client_Validation_Test_EntityDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Client_Validation_Test_Entity" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteClientValidationTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_Client_Validation_Test_EntityMutation,
  Delete_Client_Validation_Test_EntityMutationVariables
>;
export const Get_Client_Validation_Test_EntityDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Client_Validation_Test_Entity" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "clientValidationTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "businessEmail" },
                },
                { kind: "Field", name: { kind: "Name", value: "email" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "eulaAccepted" },
                },
                { kind: "Field", name: { kind: "Name", value: "future" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "futureOrPresent" },
                },
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "length" } },
                { kind: "Field", name: { kind: "Name", value: "negative" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "negativeOrZero" },
                },
                { kind: "Field", name: { kind: "Name", value: "notBlank" } },
                { kind: "Field", name: { kind: "Name", value: "notEmpty" } },
                { kind: "Field", name: { kind: "Name", value: "nullField" } },
                { kind: "Field", name: { kind: "Name", value: "past" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "pastOrPresent" },
                },
                { kind: "Field", name: { kind: "Name", value: "pattern" } },
                { kind: "Field", name: { kind: "Name", value: "positive" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "positiveOrZero" },
                },
                { kind: "Field", name: { kind: "Name", value: "price" } },
                { kind: "Field", name: { kind: "Name", value: "quantity" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "requiredWithoutDirective" },
                },
                { kind: "Field", name: { kind: "Name", value: "size" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "trialExpired" },
                },
                { kind: "Field", name: { kind: "Name", value: "urlString" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "urlWithoutDirective" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Client_Validation_Test_EntityQuery,
  Get_Client_Validation_Test_EntityQueryVariables
>;
export const Update_Client_Validation_Test_EntityDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Client_Validation_Test_Entity" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "ClientValidationTestEntityInput" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateClientValidationTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Client_Validation_Test_EntityMutation,
  Update_Client_Validation_Test_EntityMutationVariables
>;
export const Delete_OwnerDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Owner" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteOwner" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_OwnerMutation,
  Delete_OwnerMutationVariables
>;
export const Get_OwnerDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "owner" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "firstName" } },
                { kind: "Field", name: { kind: "Name", value: "lastName" } },
                { kind: "Field", name: { kind: "Name", value: "city" } },
                { kind: "Field", name: { kind: "Name", value: "address" } },
                { kind: "Field", name: { kind: "Name", value: "telephone" } },
                { kind: "Field", name: { kind: "Name", value: "email" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_OwnerQuery, Get_OwnerQueryVariables>;
export const Update_OwnerDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Owner" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OwnerInputDTO" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateOwner" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_OwnerMutation,
  Update_OwnerMutationVariables
>;
export const Get_Owner_List_With_FilterDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner_List_With_Filter" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "filter" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OwnerFilterInput" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "ownerByNamesList" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "filter" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "filter" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "firstName" } },
                { kind: "Field", name: { kind: "Name", value: "lastName" } },
                { kind: "Field", name: { kind: "Name", value: "city" } },
                { kind: "Field", name: { kind: "Name", value: "address" } },
                { kind: "Field", name: { kind: "Name", value: "telephone" } },
                { kind: "Field", name: { kind: "Name", value: "email" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Owner_List_With_FilterQuery,
  Get_Owner_List_With_FilterQueryVariables
>;
export const Get_Owner_List_With_Filter_Page_SortDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner_List_With_Filter_Page_Sort" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "filter" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OwnerFilterInput" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "page" } },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OffsetPageInput" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "sort" } },
          type: {
            kind: "ListType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "OwnerOrderByInput" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "ownerList" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "filter" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "filter" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "page" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "page" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "sort" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "sort" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "content" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "firstName" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "lastName" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "city" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "address" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "telephone" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "email" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "totalElements" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Owner_List_With_Filter_Page_SortQuery,
  Get_Owner_List_With_Filter_Page_SortQueryVariables
>;
export const Get_Owner_List_With_PaginationDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner_List_With_Pagination" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "page" } },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OffsetPageInput" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "ownerListOffsetPage" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "page" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "page" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "content" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "firstName" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "lastName" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "city" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "address" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "telephone" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "email" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "totalElements" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Owner_List_With_PaginationQuery,
  Get_Owner_List_With_PaginationQueryVariables
>;
export const Get_Owner_List_With_SortDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner_List_With_Sort" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "sort" } },
          type: {
            kind: "ListType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "OwnerOrderByInput" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "ownerListSorted" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "sort" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "sort" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "firstName" } },
                { kind: "Field", name: { kind: "Name", value: "lastName" } },
                { kind: "Field", name: { kind: "Name", value: "city" } },
                { kind: "Field", name: { kind: "Name", value: "address" } },
                { kind: "Field", name: { kind: "Name", value: "telephone" } },
                { kind: "Field", name: { kind: "Name", value: "email" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Owner_List_With_SortQuery,
  Get_Owner_List_With_SortQueryVariables
>;
export const Get_Pet_List_With_FilterDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_List_With_Filter" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "identificationNumber" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petByIdentificationNumberList" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "identificationNumber" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "identificationNumber" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "identificationNumber" },
                },
                { kind: "Field", name: { kind: "Name", value: "birthDate" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "type" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "owner" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "firstName" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "lastName" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "description" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "description" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tags" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "diseases" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "description" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_List_With_FilterQuery,
  Get_Pet_List_With_FilterQueryVariables
>;
export const Delete_PetDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Pet" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deletePet" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Delete_PetMutation, Delete_PetMutationVariables>;
export const Get_PetDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "pet" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "identificationNumber" },
                },
                { kind: "Field", name: { kind: "Name", value: "birthDate" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "type" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "owner" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "firstName" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "lastName" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "description" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "description" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tags" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "diseases" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "description" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_PetQuery, Get_PetQueryVariables>;
export const Update_PetDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Pet" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "PetInputDTO" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updatePet" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Update_PetMutation, Update_PetMutationVariables>;
export const Delete_Pet_DiseaseDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Pet_Disease" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deletePetDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_Pet_DiseaseMutation,
  Delete_Pet_DiseaseMutationVariables
>;
export const Get_Pet_Disease_CardsEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Disease_CardsEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Disease_CardsEditorQuery,
  Get_Pet_Disease_CardsEditorQueryVariables
>;
export const Update_Pet_Disease_CardsEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Pet_Disease_CardsEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "PetDiseaseInputDTO" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updatePetDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Pet_Disease_CardsEditorMutation,
  Update_Pet_Disease_CardsEditorMutationVariables
>;
export const Get_Pet_Disease_ListEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Disease_ListEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Disease_ListEditorQuery,
  Get_Pet_Disease_ListEditorQueryVariables
>;
export const Update_Pet_Disease_ListEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Pet_Disease_ListEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "PetDiseaseInputDTO" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updatePetDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Pet_Disease_ListEditorMutation,
  Update_Pet_Disease_ListEditorMutationVariables
>;
export const Get_Pet_Disease_TableEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Disease_TableEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Disease_TableEditorQuery,
  Get_Pet_Disease_TableEditorQueryVariables
>;
export const Update_Pet_Disease_TableEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Pet_Disease_TableEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "PetDiseaseInputDTO" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updatePetDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Pet_Disease_TableEditorMutation,
  Update_Pet_Disease_TableEditorMutationVariables
>;
export const Delete_Pet_TypeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Pet_Type" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deletePetType" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_Pet_TypeMutation,
  Delete_Pet_TypeMutationVariables
>;
export const Get_Pet_TypeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Type" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petType" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "defenseStatus" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_Pet_TypeQuery, Get_Pet_TypeQueryVariables>;
export const Update_Pet_TypeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Pet_Type" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "PetTypeInputDTO" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updatePetType" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Pet_TypeMutation,
  Update_Pet_TypeMutationVariables
>;
export const Get_Scalars_Test_Entity_List_With_Filter_Page_SortDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: {
        kind: "Name",
        value: "Get_Scalars_Test_Entity_List_With_Filter_Page_Sort",
      },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "filter" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "ScalarsTestEntityFilterInput" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "page" } },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OffsetPageInput" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "sort" } },
          type: {
            kind: "ListType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "ScalarsTestEntityOrderByInput" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "scalarsTestEntityList" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "filter" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "filter" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "page" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "page" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "sort" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "sort" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "content" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "intTest" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "intPrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "byteTest" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "bytePrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "shortTest" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "shortPrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "doubleTest" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "doublePrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "floatTest" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "floatPrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "string" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "bool" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "boolPrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "bigInt" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "longTest" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "longPrimitive" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "bigDecimal" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "localDate" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "localDateTime" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "localTime" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "offsetDateTime" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "offsetTime" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "dateTest" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "url" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "totalElements" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Scalars_Test_Entity_List_With_Filter_Page_SortQuery,
  Get_Scalars_Test_Entity_List_With_Filter_Page_SortQueryVariables
>;
export const Delete_ScalarsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Scalars" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteScalarsTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_ScalarsMutation,
  Delete_ScalarsMutationVariables
>;
export const Get_ScalarsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Scalars" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "scalarsTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "intTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "intPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "byteTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "bytePrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "shortTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "shortPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "doubleTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "doublePrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "floatTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "floatPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "string" } },
                { kind: "Field", name: { kind: "Name", value: "bool" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "boolPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "bigInt" } },
                { kind: "Field", name: { kind: "Name", value: "longTest" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "longPrimitive" },
                },
                { kind: "Field", name: { kind: "Name", value: "bigDecimal" } },
                { kind: "Field", name: { kind: "Name", value: "localDate" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localDateTime" },
                },
                { kind: "Field", name: { kind: "Name", value: "localTime" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "offsetDateTime" },
                },
                { kind: "Field", name: { kind: "Name", value: "offsetTime" } },
                { kind: "Field", name: { kind: "Name", value: "dateTest" } },
                { kind: "Field", name: { kind: "Name", value: "url" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_ScalarsQuery, Get_ScalarsQueryVariables>;
export const Update_ScalarsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Scalars" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "ScalarsTestEntityInput" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateScalarsTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_ScalarsMutation,
  Update_ScalarsMutationVariables
>;
export const Delete_Nn_ScalarsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_NN_Scalars" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteNotNullScalarsTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_Nn_ScalarsMutation,
  Delete_Nn_ScalarsMutationVariables
>;
export const Get_Nn_ScalarsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_NN_Scalars" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "notNullScalarsTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "bigDecimalNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "bigIntNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "dateTestNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localDateNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localDateTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "localTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "offsetDateTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "offsetTimeNotNull" },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "stringNotNull" },
                },
                { kind: "Field", name: { kind: "Name", value: "urlNotNull" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<Get_Nn_ScalarsQuery, Get_Nn_ScalarsQueryVariables>;
export const Update_Nn_ScalarsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_NN_Scalars" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "NotNullScalarsTestEntityInput" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateNotNullScalarsTestEntity" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Nn_ScalarsMutation,
  Update_Nn_ScalarsMutationVariables
>;
export const Get_Nn_Scalars_Test_Entity_List_With_Filter_Page_SortDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: {
        kind: "Name",
        value: "Get_NN_Scalars_Test_Entity_List_With_Filter_Page_Sort",
      },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "filter" },
          },
          type: {
            kind: "NamedType",
            name: {
              kind: "Name",
              value: "NotNullScalarsTestEntityFilterInput",
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "page" } },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OffsetPageInput" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "sort" } },
          type: {
            kind: "ListType",
            type: {
              kind: "NamedType",
              name: {
                kind: "Name",
                value: "NotNullScalarsTestEntityOrderByInput",
              },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "notNullScalarsTestEntityList" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "filter" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "filter" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "page" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "page" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "sort" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "sort" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "content" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "bigDecimalNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "bigIntNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "dateTestNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "localDateNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "localDateTimeNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "localTimeNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "offsetDateTimeNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "offsetTimeNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "stringNotNull" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "urlNotNull" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "totalElements" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Nn_Scalars_Test_Entity_List_With_Filter_Page_SortQuery,
  Get_Nn_Scalars_Test_Entity_List_With_Filter_Page_SortQueryVariables
>;
export const Get_Visit_List_With_FilterDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Visit_List_With_Filter" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "filter" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "VisitFilterInput" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "visitList" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "filter" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "filter" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "pet" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "identificationNumber" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "birthDate" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "type" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                          ],
                        },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "owner" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "firstName" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "lastName" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
                { kind: "Field", name: { kind: "Name", value: "visitEnd" } },
                { kind: "Field", name: { kind: "Name", value: "visitStart" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Visit_List_With_FilterQuery,
  Get_Visit_List_With_FilterQueryVariables
>;
export const Delete_VisitDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Delete_Visit" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteVisit" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Delete_VisitMutation,
  Delete_VisitMutationVariables
>;
export const Get_Pet_Disease_ListDetailsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Disease_ListDetails" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Disease_ListDetailsQuery,
  Get_Pet_Disease_ListDetailsQueryVariables
>;
export const Get_Owner_List_With_Result_PageDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Owner_List_With_Result_Page" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "page" } },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "OffsetPageInput" },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "ownerListWithResultPage" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "page" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "page" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "content" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "firstName" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "lastName" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "city" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "address" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "telephone" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "email" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "totalElements" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Owner_List_With_Result_PageQuery,
  Get_Owner_List_With_Result_PageQueryVariables
>;
export const Get_Pet_Disease_StandaloneEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "Get_Pet_Disease_StandaloneEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "ID" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "petDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "description" } },
                { kind: "Field", name: { kind: "Name", value: "name" } },
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Get_Pet_Disease_StandaloneEditorQuery,
  Get_Pet_Disease_StandaloneEditorQueryVariables
>;
export const Update_Pet_Disease_StandaloneEditorDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "Update_Pet_Disease_StandaloneEditor" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "input" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "PetDiseaseInputDTO" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updatePetDisease" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "input" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "input" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  Update_Pet_Disease_StandaloneEditorMutation,
  Update_Pet_Disease_StandaloneEditorMutationVariables
>;
