import { OwnerCardsWithResultPage } from "../screens/standalone-collection/OwnerCardsWithResultPage";
import { StandaloneReadOnlyOwnerTableWithFilterSortPage } from "../screens/readonly-collection/StandaloneReadOnlyOwnerTableWithFilterSortPage";
import { StandaloneReadOnlyOwnerListWithFilterSortPage } from "../screens/readonly-collection/StandaloneReadOnlyOwnerListWithFilterSortPage";
import { StandaloneReadOnlyOwnerCardsWithFilterSortPage } from "../screens/readonly-collection/StandaloneReadOnlyOwnerCardsWithFilterSortPage";
import { ReadOnlyOwnerTableWithFilterSortPageScreenLayout } from "../screens/readonly-collection/ReadOnlyOwnerTableWithFilterSortPageScreenLayout";
import { ReadOnlyOwnerListWithFilterSortPageScreenLayout } from "../screens/readonly-collection/ReadOnlyOwnerListWithFilterSortPageScreenLayout";
import { ReadOnlyOwnerCardsWithFilterSortPageScreenLayout } from "../screens/readonly-collection/ReadOnlyOwnerCardsWithFilterSortPageScreenLayout";
import { VisitWithFilterScreenLayout } from "../screens/management/VisitWithFilterScreenLayout";
import { ScalarsCardsWithFilterSortPageScreenLayout } from "../screens/management/ScalarsCardsWithFilterSortPageScreenLayout";
import { ScalarsNotNullCardsWithFilterSortPageScreenLayout } from "../screens/management/ScalarsNotNullCardsWithFilterSortPageScreenLayout";
import { OwnerTableWithPageScreenLayout } from "../screens/management/OwnerTableWithPageScreenLayout";
import { OwnerTableWithSortScreenLayout } from "../screens/management/OwnerTableWithSortScreenLayout";
import { OwnerTableWithFilterScreenLayout } from "../screens/management/OwnerTableWithFilterScreenLayout";
import { OwnerTableWithFilterSortPageScreenLayout } from "../screens/management/OwnerTableWithFilterSortPageScreenLayout";
import { OwnerListWithPageScreenLayout } from "../screens/management/OwnerListWithPageScreenLayout";
import { OwnerListWithSortScreenLayout } from "../screens/management/OwnerListWithSortScreenLayout";
import { OwnerListWithFilterScreenLayout } from "../screens/management/OwnerListWithFilterScreenLayout";
import { OwnerListsWithFilterSortPageScreenLayout } from "../screens/management/OwnerListsWithFilterSortPageScreenLayout";
import { OwnerCardsWithPageScreenLayout } from "../screens/management/OwnerCardsWithPageScreenLayout";
import { OwnerCardsWithSortScreenLayout } from "../screens/management/OwnerCardsWithSortScreenLayout";
import { OwnerCardsWithFilterScreenLayout } from "../screens/management/OwnerCardsWithFilterScreenLayout";
import { OwnerCardsWithFilterSortPageScreenLayout } from "../screens/management/OwnerCardsWithFilterSortPageScreenLayout";
import { OwnerTableWithMultiselectScreenLayout } from "../screens/management/OwnerTableWithMultiselectScreenLayout";
import { StandaloneReadOnlyPetTable } from "../screens/no-details-readonly-collection/StandaloneReadOnlyPetTable";
import { StandaloneReadOnlyPetList } from "../screens/no-details-readonly-collection/StandaloneReadOnlyPetList";
import { StandaloneReadOnlyPetCards } from "../screens/no-details-readonly-collection/StandaloneReadOnlyPetCards";
import { StandaloneScalarsCards } from "../screens/standalone-collection/StandaloneScalarsCards";
import { StandalonePetDiseaseList } from "../screens/standalone-collection/StandalonePetDiseaseList";
import { StandaloneOwnerTable } from "../screens/standalone-collection/StandaloneOwnerTable";
import { StandaloneOwnerCards } from "../screens/standalone-collection/StandaloneOwnerCards";
import { StandaloneOwnerList } from "../screens/standalone-collection/StandaloneOwnerList";
import { ReadOnlyPetDiseaseListScreenLayout } from "../screens/readonly-collection/ReadOnlyPetDiseaseListScreenLayout";
import { ReadOnlyScalarsListScreenLayout } from "../screens/readonly-collection/ReadOnlyScalarsListScreenLayout";
import { ReadOnlyPetTableScreenLayout } from "../screens/readonly-collection/ReadOnlyPetTableScreenLayout";
import { ReadOnlyPetCardsScreenLayout } from "../screens/readonly-collection/ReadOnlyPetCardsScreenLayout";
import { ReadOnlyPetListScreenLayout } from "../screens/readonly-collection/ReadOnlyPetListScreenLayout";
import { ReadOnlyOwnerTableScreenLayout } from "../screens/readonly-collection/ReadOnlyOwnerTableScreenLayout";
import { ReadOnlyOwnerCardsScreenLayout } from "../screens/readonly-collection/ReadOnlyOwnerCardsScreenLayout";
import { ReadOnlyOwnerListScreenLayout } from "../screens/readonly-collection/ReadOnlyOwnerListScreenLayout";
import { ClientValidationTestEntityCardsScreenLayout } from "../screens/management/ClientValidationTestEntityCardsScreenLayout";
import { PetTypeTableScreenLayout } from "../screens/management/PetTypeTableScreenLayout";
import { PetDiseaseTableScreenLayout } from "../screens/management/PetDiseaseTableScreenLayout";
import { PetDiseaseCardsScreenLayout } from "../screens/management/PetDiseaseCardsScreenLayout";
import { PetDiseaseListScreenLayout } from "../screens/management/PetDiseaseListScreenLayout";
import { ScalarsNotNullCardsScreenLayout } from "../screens/management/ScalarsNotNullCardsScreenLayout";
import { ScalarsTableScreenLayout } from "../screens/management/ScalarsTableScreenLayout";
import { ScalarsListScreenLayout } from "../screens/management/ScalarsListScreenLayout";
import { PetTableScreenLayout } from "../screens/management/PetTableScreenLayout";
import { PetCardsScreenLayout } from "../screens/management/PetCardsScreenLayout";
import { PetListScreenLayout } from "../screens/management/PetListScreenLayout";
import { OwnerTableScreenLayout } from "../screens/management/OwnerTableScreenLayout";
import { OwnerCardsScreenLayout } from "../screens/management/OwnerCardsScreenLayout";
import { OwnerListScreenLayout } from "../screens/management/OwnerListScreenLayout";
import { PetDescriptionLookupCards } from "../screens/lookup/PetDescriptionLookupCards";
import { ScalarsNotNullLookupCards } from "../screens/lookup/ScalarsNotNullLookupCards";
import { ScalarsLookupCards } from "../screens/lookup/ScalarsLookupCards";
import { PetDiseaseLookupCards } from "../screens/lookup/PetDiseaseLookupCards";
import { PetTypeLookupCards } from "../screens/lookup/PetTypeLookupCards";
import { PetLookupCards } from "../screens/lookup/PetLookupCards";
import { OwnerLookupCards } from "../screens/lookup/OwnerLookupCards";
import { BlankComponent } from "../screens/blank/BlankComponent";
import { Routes, Route } from "react-router-dom";
import { Home } from "../screens/home/Home";
import React from "react";
import { Page404 } from "../../core/routing/Page404";
import { AuthRedirect } from "../../core/security/AuthRedirect";

export function AppRoutes() {
  return (
    <Routes>
      <Route path="auth" element={<AuthRedirect />} />
      <Route path="/" element={<Home />} />
      <Route path="*" element={<Page404 />} />
      <Route path="blank-component" element={<BlankComponent />} />
      <Route path="owner-lookup-cards">
        <Route index element={<OwnerLookupCards />} />
        <Route path=":recordId" element={<OwnerLookupCards />} />
      </Route>
      <Route path="pet-lookup-cards">
        <Route index element={<PetLookupCards />} />
        <Route path=":recordId" element={<PetLookupCards />} />
      </Route>
      <Route path="pet-type-lookup-cards">
        <Route index element={<PetTypeLookupCards />} />
        <Route path=":recordId" element={<PetTypeLookupCards />} />
      </Route>
      <Route path="pet-disease-lookup-cards">
        <Route index element={<PetDiseaseLookupCards />} />
        <Route path=":recordId" element={<PetDiseaseLookupCards />} />
      </Route>
      <Route path="scalars-lookup-cards">
        <Route index element={<ScalarsLookupCards />} />
        <Route path=":recordId" element={<ScalarsLookupCards />} />
      </Route>
      <Route path="scalars-not-null-lookup-cards">
        <Route index element={<ScalarsNotNullLookupCards />} />
        <Route path=":recordId" element={<ScalarsNotNullLookupCards />} />
      </Route>
      <Route path="pet-description-lookup-cards">
        <Route index element={<PetDescriptionLookupCards />} />
        <Route path=":recordId" element={<PetDescriptionLookupCards />} />
      </Route>
      <Route path="owner-list">
        <Route index element={<OwnerListScreenLayout />} />
        <Route path=":recordId" element={<OwnerListScreenLayout />} />
      </Route>
      <Route path="owner-cards">
        <Route index element={<OwnerCardsScreenLayout />} />
        <Route path=":recordId" element={<OwnerCardsScreenLayout />} />
      </Route>
      <Route path="owner-table">
        <Route index element={<OwnerTableScreenLayout />} />
        <Route path=":recordId" element={<OwnerTableScreenLayout />} />
      </Route>
      <Route path="pet-list">
        <Route index element={<PetListScreenLayout />} />
        <Route path=":recordId" element={<PetListScreenLayout />} />
      </Route>
      <Route path="pet-cards">
        <Route index element={<PetCardsScreenLayout />} />
        <Route path=":recordId" element={<PetCardsScreenLayout />} />
      </Route>
      <Route path="pet-table">
        <Route index element={<PetTableScreenLayout />} />
        <Route path=":recordId" element={<PetTableScreenLayout />} />
      </Route>
      <Route path="scalars-list">
        <Route index element={<ScalarsListScreenLayout />} />
        <Route path=":recordId" element={<ScalarsListScreenLayout />} />
      </Route>
      <Route path="scalars-table">
        <Route index element={<ScalarsTableScreenLayout />} />
        <Route path=":recordId" element={<ScalarsTableScreenLayout />} />
      </Route>
      <Route path="scalars-not-null-cards">
        <Route index element={<ScalarsNotNullCardsScreenLayout />} />
        <Route path=":recordId" element={<ScalarsNotNullCardsScreenLayout />} />
      </Route>
      <Route path="pet-disease-list">
        <Route index element={<PetDiseaseListScreenLayout />} />
        <Route path=":recordId" element={<PetDiseaseListScreenLayout />} />
      </Route>
      <Route path="pet-disease-cards">
        <Route index element={<PetDiseaseCardsScreenLayout />} />
        <Route path=":recordId" element={<PetDiseaseCardsScreenLayout />} />
      </Route>
      <Route path="pet-disease-table">
        <Route index element={<PetDiseaseTableScreenLayout />} />
        <Route path=":recordId" element={<PetDiseaseTableScreenLayout />} />
      </Route>
      <Route path="pet-type-table">
        <Route index element={<PetTypeTableScreenLayout />} />
        <Route path=":recordId" element={<PetTypeTableScreenLayout />} />
      </Route>
      <Route path="client-validation-test-entity-cards">
        <Route
          index
          element={<ClientValidationTestEntityCardsScreenLayout />}
        />
        <Route
          path=":recordId"
          element={<ClientValidationTestEntityCardsScreenLayout />}
        />
      </Route>
      <Route path="read-only-owner-list">
        <Route index element={<ReadOnlyOwnerListScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyOwnerListScreenLayout />} />
      </Route>
      <Route path="read-only-owner-cards">
        <Route index element={<ReadOnlyOwnerCardsScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyOwnerCardsScreenLayout />} />
      </Route>
      <Route path="read-only-owner-table">
        <Route index element={<ReadOnlyOwnerTableScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyOwnerTableScreenLayout />} />
      </Route>
      <Route path="read-only-pet-list">
        <Route index element={<ReadOnlyPetListScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyPetListScreenLayout />} />
      </Route>
      <Route path="read-only-pet-cards">
        <Route index element={<ReadOnlyPetCardsScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyPetCardsScreenLayout />} />
      </Route>
      <Route path="read-only-pet-table">
        <Route index element={<ReadOnlyPetTableScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyPetTableScreenLayout />} />
      </Route>
      <Route path="read-only-scalars-list">
        <Route index element={<ReadOnlyScalarsListScreenLayout />} />
        <Route path=":recordId" element={<ReadOnlyScalarsListScreenLayout />} />
      </Route>
      <Route path="read-only-pet-disease-list">
        <Route index element={<ReadOnlyPetDiseaseListScreenLayout />} />
        <Route
          path=":recordId"
          element={<ReadOnlyPetDiseaseListScreenLayout />}
        />
      </Route>
      <Route path="standalone-owner-list">
        <Route index element={<StandaloneOwnerList />} />
        <Route path=":recordId" element={<StandaloneOwnerList />} />
      </Route>
      <Route path="standalone-owner-cards">
        <Route index element={<StandaloneOwnerCards />} />
        <Route path=":recordId" element={<StandaloneOwnerCards />} />
      </Route>
      <Route path="standalone-owner-table">
        <Route index element={<StandaloneOwnerTable />} />
        <Route path=":recordId" element={<StandaloneOwnerTable />} />
      </Route>
      <Route path="standalone-pet-disease-list">
        <Route index element={<StandalonePetDiseaseList />} />
        <Route path=":recordId" element={<StandalonePetDiseaseList />} />
      </Route>
      <Route path="standalone-scalars-cards">
        <Route index element={<StandaloneScalarsCards />} />
        <Route path=":recordId" element={<StandaloneScalarsCards />} />
      </Route>
      <Route path="standalone-read-only-pet-cards">
        <Route index element={<StandaloneReadOnlyPetCards />} />
        <Route path=":recordId" element={<StandaloneReadOnlyPetCards />} />
      </Route>
      <Route path="standalone-read-only-pet-list">
        <Route index element={<StandaloneReadOnlyPetList />} />
        <Route path=":recordId" element={<StandaloneReadOnlyPetList />} />
      </Route>
      <Route path="standalone-read-only-pet-table">
        <Route index element={<StandaloneReadOnlyPetTable />} />
        <Route path=":recordId" element={<StandaloneReadOnlyPetTable />} />
      </Route>
      <Route path="owner-table-with-multiselect">
        <Route index element={<OwnerTableWithMultiselectScreenLayout />} />
        <Route
          path=":recordId"
          element={<OwnerTableWithMultiselectScreenLayout />}
        />
      </Route>
      <Route path="owner-cards-with-filter-sort-page">
        <Route index element={<OwnerCardsWithFilterSortPageScreenLayout />} />
        <Route
          path=":recordId"
          element={<OwnerCardsWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="owner-cards-with-filter">
        <Route index element={<OwnerCardsWithFilterScreenLayout />} />
        <Route
          path=":recordId"
          element={<OwnerCardsWithFilterScreenLayout />}
        />
      </Route>
      <Route path="owner-cards-with-sort">
        <Route index element={<OwnerCardsWithSortScreenLayout />} />
        <Route path=":recordId" element={<OwnerCardsWithSortScreenLayout />} />
      </Route>
      <Route path="owner-cards-with-page">
        <Route index element={<OwnerCardsWithPageScreenLayout />} />
        <Route path=":recordId" element={<OwnerCardsWithPageScreenLayout />} />
      </Route>
      <Route path="owner-lists-with-filter-sort-page">
        <Route index element={<OwnerListsWithFilterSortPageScreenLayout />} />
        <Route
          path=":recordId"
          element={<OwnerListsWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="owner-list-with-filter">
        <Route index element={<OwnerListWithFilterScreenLayout />} />
        <Route path=":recordId" element={<OwnerListWithFilterScreenLayout />} />
      </Route>
      <Route path="owner-list-with-sort">
        <Route index element={<OwnerListWithSortScreenLayout />} />
        <Route path=":recordId" element={<OwnerListWithSortScreenLayout />} />
      </Route>
      <Route path="owner-list-with-page">
        <Route index element={<OwnerListWithPageScreenLayout />} />
        <Route path=":recordId" element={<OwnerListWithPageScreenLayout />} />
      </Route>
      <Route path="owner-table-with-filter-sort-page">
        <Route index element={<OwnerTableWithFilterSortPageScreenLayout />} />
        <Route
          path=":recordId"
          element={<OwnerTableWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="owner-table-with-filter">
        <Route index element={<OwnerTableWithFilterScreenLayout />} />
        <Route
          path=":recordId"
          element={<OwnerTableWithFilterScreenLayout />}
        />
      </Route>
      <Route path="owner-table-with-sort">
        <Route index element={<OwnerTableWithSortScreenLayout />} />
        <Route path=":recordId" element={<OwnerTableWithSortScreenLayout />} />
      </Route>
      <Route path="owner-table-with-page">
        <Route index element={<OwnerTableWithPageScreenLayout />} />
        <Route path=":recordId" element={<OwnerTableWithPageScreenLayout />} />
      </Route>
      <Route path="scalars-not-null-cards-with-filter-sort-page">
        <Route
          index
          element={<ScalarsNotNullCardsWithFilterSortPageScreenLayout />}
        />
        <Route
          path=":recordId"
          element={<ScalarsNotNullCardsWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="scalars-cards-with-filter-sort-page">
        <Route index element={<ScalarsCardsWithFilterSortPageScreenLayout />} />
        <Route
          path=":recordId"
          element={<ScalarsCardsWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="visit-with-filter">
        <Route index element={<VisitWithFilterScreenLayout />} />
        <Route path=":recordId" element={<VisitWithFilterScreenLayout />} />
      </Route>
      <Route path="read-only-owner-cards-with-filter-sort-page">
        <Route
          index
          element={<ReadOnlyOwnerCardsWithFilterSortPageScreenLayout />}
        />
        <Route
          path=":recordId"
          element={<ReadOnlyOwnerCardsWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="read-only-owner-list-with-filter-sort-page">
        <Route
          index
          element={<ReadOnlyOwnerListWithFilterSortPageScreenLayout />}
        />
        <Route
          path=":recordId"
          element={<ReadOnlyOwnerListWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="read-only-owner-table-with-filter-sort-page">
        <Route
          index
          element={<ReadOnlyOwnerTableWithFilterSortPageScreenLayout />}
        />
        <Route
          path=":recordId"
          element={<ReadOnlyOwnerTableWithFilterSortPageScreenLayout />}
        />
      </Route>
      <Route path="standalone-read-only-owner-cards-with-filter-sort-page">
        <Route
          index
          element={<StandaloneReadOnlyOwnerCardsWithFilterSortPage />}
        />
        <Route
          path=":recordId"
          element={<StandaloneReadOnlyOwnerCardsWithFilterSortPage />}
        />
      </Route>
      <Route path="standalone-read-only-owner-list-with-filter-sort-page">
        <Route
          index
          element={<StandaloneReadOnlyOwnerListWithFilterSortPage />}
        />
        <Route
          path=":recordId"
          element={<StandaloneReadOnlyOwnerListWithFilterSortPage />}
        />
      </Route>
      <Route path="standalone-read-only-owner-table-with-filter-sort-page">
        <Route
          index
          element={<StandaloneReadOnlyOwnerTableWithFilterSortPage />}
        />
        <Route
          path=":recordId"
          element={<StandaloneReadOnlyOwnerTableWithFilterSortPage />}
        />
      </Route>
      <Route path="owner-cards-with-result-page">
        <Route index element={<OwnerCardsWithResultPage />} />
        <Route path=":recordId" element={<OwnerCardsWithResultPage />} />
      </Route>
    </Routes>
  );
}
