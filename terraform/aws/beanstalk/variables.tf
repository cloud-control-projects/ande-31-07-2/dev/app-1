variable "name" {
  type        = string
  description = "Application name"
}

variable "use_private_subnets" {
  type = bool
}

variable "azs_count" {
  type        = number
  description = "Number of Availability Zones to be used"
}

variable "instance_type" {
  type = string
}

variable "region" {
  type = string
}

variable "image" {
  type = string
}

variable "ports" {
  type = list(number)
}

variable "env" {
  type = list(object({
    namespace = string,
    name      = string
    value     = any
  }))
}

variable "https_enabled" {
  type    = bool
  default = false
}

variable "cname_prefix" {
  default = null
}

variable "module_path" {
  type = string
}

variable "enable_logging" {
  type    = bool
  default = false
}

variable "delete_logs_on_terminate" {
  type    = bool
  default = false
}

variable "logs_retention" {
  type    = number
  default = 30
}

variable "collect_custom_metrics" {
  type    = bool
  default = false
}

variable "vpc_id" {}

variable "private_subnets" {}

variable "public_subnets" {}
