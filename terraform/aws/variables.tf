variable "name" {
  type        = string
  default     = "app1"
  description = "Application name"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "use_private_subnets" {
  type    = bool
  default = false
}

variable "azs_count" {
  type        = number
  default     = 2
  description = "Number of Availability Zones to be used"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

variable "image" {
  type = string
}

variable "ports" {
  type    = list(number)
  default = [8080]
}

locals {
  env = [
    {
      namespace = "aws:elbv2:listener:443",
      name      = "ListenerEnabled",
      value     = "true"
    },
    {
      namespace = "aws:elbv2:listener:443",
      name      = "Protocol",
      value     = "HTTPS"
    },
    {
      namespace = "aws:elbv2:listener:443",
      name      = "SSLCertificateArns",
      value     = module.ssl.certificate_arn
    },
    {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "StreamLogs"
      value     = var.enable_logging
    },
    {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "DeleteOnTerminate"
      value     = var.delete_logs_on_terminate
    },
    {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "RetentionInDays"
      value     = var.logs_retention
    },
  ]
}

variable "enable_logging" {
  type    = bool
  default = true
}

variable "delete_logs_on_terminate" {
  type    = bool
  default = true
}

variable "logs_retention" {
  type    = number
  default = 3
}

    