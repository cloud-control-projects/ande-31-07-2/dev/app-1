package io.jmix2mvp.petclinic.entity.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PetTestConstraintValidator2.class)
@Documented
public @interface PetTestConstraint2 {
    String message() default "{Pet.PetTestConstraint2}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}