package io.jmix2mvp.petclinic.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.time.*;
import java.util.Date;

public class NotNullScalarsTestEntityFilter {

    private String stringNotNull;
    private BigInteger bigIntNotNullMin;
    private BigDecimal bigDecimalNotNullMin;
    private LocalDate localDateNotNullMax;
    private OffsetTime offsetTimeNotNullMax;
    private OffsetDateTime offsetDateTimeNotNullMin;
    private OffsetDateTime offsetDateTimeNotNullMax;
    private LocalTime localTimeNotNull;
    private LocalDateTime localDateTimeNotNullMin;
    private Date dateTestNotNullMin;
    private URL urlNotNull;

    public String getStringNotNull() {
        return stringNotNull;
    }

    public void setStringNotNull(String stringNotNull) {
        this.stringNotNull = stringNotNull;
    }

    public BigInteger getBigIntNotNullMin() {
        return bigIntNotNullMin;
    }

    public void setBigIntNotNullMin(BigInteger bigIntNotNullMin) {
        this.bigIntNotNullMin = bigIntNotNullMin;
    }

    public BigDecimal getBigDecimalNotNullMin() {
        return bigDecimalNotNullMin;
    }

    public void setBigDecimalNotNullMin(BigDecimal bigDecimalNotNullMin) {
        this.bigDecimalNotNullMin = bigDecimalNotNullMin;
    }

    public LocalDate getLocalDateNotNullMax() {
        return localDateNotNullMax;
    }

    public void setLocalDateNotNullMax(LocalDate localDateNotNullMax) {
        this.localDateNotNullMax = localDateNotNullMax;
    }

    public OffsetTime getOffsetTimeNotNullMax() {
        return offsetTimeNotNullMax;
    }

    public void setOffsetTimeNotNullMax(OffsetTime offsetTimeNotNullMax) {
        this.offsetTimeNotNullMax = offsetTimeNotNullMax;
    }

    public OffsetDateTime getOffsetDateTimeNotNullMin() {
        return offsetDateTimeNotNullMin;
    }

    public void setOffsetDateTimeNotNullMin(OffsetDateTime offsetDateTimeNotNullMin) {
        this.offsetDateTimeNotNullMin = offsetDateTimeNotNullMin;
    }

    public OffsetDateTime getOffsetDateTimeNotNullMax() {
        return offsetDateTimeNotNullMax;
    }

    public void setOffsetDateTimeNotNullMax(OffsetDateTime offsetDateTimeNotNullMax) {
        this.offsetDateTimeNotNullMax = offsetDateTimeNotNullMax;
    }

    public LocalTime getLocalTimeNotNull() {
        return localTimeNotNull;
    }

    public void setLocalTimeNotNull(LocalTime localTimeNotNull) {
        this.localTimeNotNull = localTimeNotNull;
    }

    public LocalDateTime getLocalDateTimeNotNullMin() {
        return localDateTimeNotNullMin;
    }

    public void setLocalDateTimeNotNullMin(LocalDateTime localDateTimeNotNullMin) {
        this.localDateTimeNotNullMin = localDateTimeNotNullMin;
    }

    public Date getDateTestNotNullMin() {
        return dateTestNotNullMin;
    }

    public void setDateTestNotNullMin(Date dateTestNotNullMin) {
        this.dateTestNotNullMin = dateTestNotNullMin;
    }

    public URL getUrlNotNull() {
        return urlNotNull;
    }

    public void setUrlNotNull(URL urlNotNull) {
        this.urlNotNull = urlNotNull;
    }
}
