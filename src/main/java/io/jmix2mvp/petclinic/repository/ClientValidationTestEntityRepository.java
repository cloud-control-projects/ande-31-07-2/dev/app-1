package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.ClientValidationTestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientValidationTestEntityRepository extends JpaRepository<ClientValidationTestEntity, Long> {
}
