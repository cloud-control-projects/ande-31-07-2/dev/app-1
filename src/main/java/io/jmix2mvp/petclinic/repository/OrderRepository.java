package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.graphql.data.GraphQlRepository;

@GraphQlRepository
public interface OrderRepository extends JpaRepository<Order, Long> {
}