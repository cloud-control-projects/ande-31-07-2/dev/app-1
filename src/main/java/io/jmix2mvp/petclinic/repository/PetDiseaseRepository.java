package io.jmix2mvp.petclinic.repository;

import io.jmix2mvp.petclinic.entity.PetDisease;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PetDiseaseRepository extends JpaRepository<PetDisease, Long>, JpaSpecificationExecutor<PetDisease> {
}
