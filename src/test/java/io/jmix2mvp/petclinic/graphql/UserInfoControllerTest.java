package io.jmix2mvp.petclinic.graphql;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.graphql.test.tester.ExecutionGraphQlServiceTester;
import org.springframework.security.test.context.support.WithMockUser;

import static io.jmix2mvp.petclinic.Authorities.VETERINARIAN;

@SpringBootTest
@AutoConfigureGraphQlTester
@WithMockUser(username = "vet", authorities = {VETERINARIAN})
public class UserInfoControllerTest {

    @Autowired
    private ExecutionGraphQlServiceTester graphQlTester;

    @Test
    public void testUserInfoSuccessful() {
        graphQlTester.documentName("userInfo")
                .execute()
                .path("userInfo.username")
                .entity(String.class)
                .satisfies(returnedValue -> {
                    Assertions.assertEquals("vet", returnedValue);
                });
    }

}
